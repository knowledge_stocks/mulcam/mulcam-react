export default function MapEx() {
    const season = ['봄', '여름', '가을', '겨울']
    // li에 key값 안 넣으면 콘솔창에서 경고 띄운다.
    const seasonList = season.map((v, i) => <li key={i}>{v}</li>)
    return (
        <ul>{seasonList}</ul>
    );
};