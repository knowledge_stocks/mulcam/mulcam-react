import { useState, useReducer } from "react";

const MapEx2 = function() {
    const [seasons, setSeasons] = useState(
        new Map([
            [1, '봄'],
            [2, '여름'],
            [3, '가을'],
            [4, '겨울'],
        ])
    );

    // useState는 내부적으로 useReducer를 wrapping한다고 한다.
    // const forceUpdate = useState()[1].bind(null, {})
    const forceUpdate = useReducer(() => ({}))[1]

    const [inputText, setInputText] = useState('');
    const [nextId, setNextId] = useState(seasons.size + 1);

    // li에 key값 안 넣으면 콘솔창에서 경고 띄운다.
    const seasonList = [...seasons].map(([key, value]) => <li onDoubleClick={() => removeSeason(key)} key={key}>{value}</li>)

    const onInputTextChanged = function(e) {
        setInputText(e.target.value);
    }

    const onAddClicked = function() {
        // seasons.push({id: nextId, inputText});
        //
        // 이건 아무런 효과가 없다. seasons가 이전 객체와 동일하기 때문에 업데이트가 안 된다.
        // setSeasons(seasons);
        // setNextId(nextId + 1); // 이거때문에 seasons도 업데이트 된다.

        // 따라서 새로운 seasons를 새로 생성해서 업데이트하는 것이 바람직하다.
        seasons.set(nextId, inputText);
        // forceUpdate(); // 요걸로 강제로 화면을 업데이트 하도록 할 수 있다.
        setNextId(nextId + 1);
        //setSeasons(seasons);
    }

    const removeSeason = function(id) {
        // const newSeasons = seasons.filter(season => season.id !== id);
        // setSeasons(newSeasons);
        seasons.delete(id);
        forceUpdate();
    }

    return (
        <>
            <input type="text" value={inputText} onChange={onInputTextChanged} />
            <button onClick={onAddClicked}>추가</button>
            <ul>{seasonList}</ul>
        </>
    );
};

export default MapEx2;