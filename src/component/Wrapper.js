export default function Wrapper({children}) {
    const style={
        border: '1px solid black',
        color: 'red',
        fontSize: '40px',
    }
    return (
        <div style={style}>
            {children}
        </div>
    )
};