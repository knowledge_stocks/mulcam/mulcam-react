import { useState } from 'react';

console.log('여기도 클라이언트 사이드 콘솔');

export default function EventEx() {
    const [name, setName] = useState('홍길동');
    const [value, setValue] = useState('기본값');
    function showName() {
        alert(name);
        console.log(name); // 클라이언트 사이드의 콘솔 = 브라우저의 콘솔에 출력됨
    }
    function showText(event) {
        setValue(event.target.value);
    }
    return (
        <div>
            <button onClick={showName}>Show name</button>
            <input type="text" onChange={showText} />
            <p>{value}</p>
        </div>
    );
};