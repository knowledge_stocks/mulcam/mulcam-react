import './JsxEx.css';
import { Fragment } from 'react';

// function JsxEx() {
//   return (
//     //여러개의 태그를 묶기 위해 무의미한 태그를 넣는 대신, Fragment나 <></>를 사용할 수 있다.
//     <>
//       <p>start</p>
//       <span>!!!!</span>
//     </>
//   );
// }

// function JsxEx() {
//   const numX = 3;
//   const numY = 5;

//   return (
//     // style을 넣을때는 중괄호를 두번 중첩해서 넣는다.
//     <>
//       <p style={{backgroundColor:'aqua'}}>{numX + numY}</p>
//     </>
//   );
// }

// function JsxEx() {
//   const name = 'react';

//   return (
//     <>
//       {name==='react' ? <h1>리액트입니다.</h1> : <h1>리액트가 아닙니다.</h1>}
//     </>
//   );
// }

// function JsxEx() {
//   const name = 'react';

//   return (
//     // {name==='react' ? <h1>참</h1> : null} 이런식으로 참일때만 화면을 보여주고 싶을 때 &&로 대체할 수 있다.
//     // &&로 다수의 조건을 판단할 경우 마지막 조건만 화면에 출력된다.
//     <>
//       {name==='react' && <h1>참</h1>}
//       {name && true && '참' && name}
//     </>
//   );
// }


// function JsxEx() {
//   const name = undefined;

//   return (
//     <>
//       {name || <h1>홍길동</h1>}
//     </>
//   );
// }

function JsxEx() {
  const name = '홍길동';

  return (
    <>
      <div className='react'>{name}</div>
    </>
  );
}

export default JsxEx;
