import PropTypes from 'prop-types';

function PropEx({name}) {
    return(
        <div>
            <h3>props:properties</h3>
            <p>Hello {name}</p>
        </div>
    );
}

// 콘솔에만 에러 뿌리고 작동은 된다.
// 웹상에서 에러를 띄우게 하려면 빌드 설정을 바꿔야 한다고 한다.
// webpack의 DefinePlugin에 production 옵션을 추가하라는데 어케 하는건지 몰겠다.
PropEx.propTypes = {
    name: PropTypes.number.isRequired,
};

PropEx.defaultProps = {
    name: '아무개'
}

export default PropEx;
