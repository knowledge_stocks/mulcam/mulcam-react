import logo from './logo.svg';
import './App.css';
import { Fragment } from 'react';
import JsxEx from './component/JsxEx.js';
import UserName from './component/UserName.js';
import EventEx from './component/EventEx';
import PropEx from './component/PropEx';
import Wrapper from './component/Wrapper';
import MapEx from './component/MapEx';
import MapEx2 from './component/MapEx2';

function App() {
  return(
    <div>
      <MapEx2/>
      <hr/>
      <MapEx/>
      <Wrapper>
        <JsxEx/><UserName name={'홍길동'}/><EventEx/><PropEx/>
      </Wrapper>
    </div>
  )
};

export default App;
